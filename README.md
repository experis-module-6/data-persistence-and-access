# Description
This project consists of two parts:

Part 1: SuperheroesDb database creation & modification

Part 2: Chinook database access & modification

Part 1 consists of 9 SQL queries to setup and modify a new database to showcase the different CRUD operations. The database is called "SuperheroesDb" and it contains information about superheroes, their assistants and superpowers.

Part 2 is a console app that accesses a sample digital mediastore database called "Chinook" (https://github.com/lerocha/chinook-database) and displays a list of operations that can be done to show and modify the data. A full list of available features is listed below.


## Features
SuperheroesDb:
- Create a new database
- Create tables
- Add reference between tables
- Insert entries
- Update entries
- Delete entries

Chinook database:
- Display all customer data
- Display a specific customer's data based on their id
- Display customer data for all customers with certain name (partial match is sufficient)
- Browse customers in page format
- Add a new customer to the database
- Update an existing customer
- Display a list of customer count per country in a descending order
- Display most popular genre(s) for a specific customer

## Usage

Part 1 - SuperheroesDb:

The queries are expected to be run in SQL Server Management Studio in the order they are numbered. Make sure that a database called "SuperheroesDb" does not exist already before running the first query.

Part 2 - Chinook database:

The sample database can be generated with the "Chinook_SqlServer_AutoIncrementPKs.sql" script which is available in the "Chinook" folder. The server name in SQL Server Management Studio connection should have a path ending in ".\SQLEXPRESS" for the app to function properly. If the path is different, it can be modified by changing the DataSource in ConnectionStringHelper.cs under ./Chinook/Repositories/

## Contributors
- Tuomo Kuusisto https://gitlab.com/pampula
- Valtteri Elo https://gitlab.com/valtterielo


## License
[MIT](https://choosealicense.com/licenses/mit/)
