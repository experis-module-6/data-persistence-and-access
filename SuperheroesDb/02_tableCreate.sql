USE SuperheroesDb

CREATE TABLE Superhero (
	id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NULL,	
	Alias nvarchar(50) NULL,
	Origin nvarchar(50)
);

CREATE TABLE Assistant (
	id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NULL
	);

CREATE TABLE Power (
	id int IDENTITY(1,1) PRIMARY KEY,
	Name nvarchar(50) NULL,
	Description nvarchar(50) NULL
	);