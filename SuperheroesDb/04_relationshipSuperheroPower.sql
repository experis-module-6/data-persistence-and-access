USE SuperheroesDb

CREATE TABLE SuperheroPower(
	Superhero_id int REFERENCES Superhero(id),
	Power_id int REFERENCES Power(id),
	PRIMARY KEY (Superhero_id, Power_id)
);