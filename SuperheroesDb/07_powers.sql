USE SuperheroesDb

INSERT INTO Power
(Name, Description)
VALUES
('Fly', 'Hero is able to fly.'),
('Immortal', 'Hero is immortal.'),
('Sarcasm', 'Understands sarcasm.'),
('Super Strength', 'Hero can move huge objects.')

INSERT INTO SuperheroPower
(Superhero_id, Power_id)
VALUES
(1,1),
(1,3),
(1,4),
(2,1),
(2,2),
(2,3),
(2,4),
(3,4)