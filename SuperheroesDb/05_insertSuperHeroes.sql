USE SuperheroesDb

INSERT INTO Superhero
(Name, Alias, Origin) 
VALUES 
('Mr. Stark', 'Ironman', 'United States'),
('Thor Odinson', 'Thor', 'Asgard'),
('Drax', 'Drax the Destroyer', 'Kylos')