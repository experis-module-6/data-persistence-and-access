﻿using System.Text;
using Chinook.Models;
using Chinook.Repositories;

namespace Chinook.Scripts
{
    public static class CustomerCountryDataHandler
    {
        private static readonly ICustomerCountryRepository Repository = new CustomerCountryRepository();

        public static void ShowCustomersByCountryDescending()
        {
            PrintCustomersByCountry(Repository.GetCustomersByCountry());
        }
        
        /// <summary>
        /// Prints out a list of customers per country in the console.
        /// </summary>
        /// <param name="customerCountries">CustomerCountry list.</param>
        /// <exception cref="Exception"></exception>
        private static void PrintCustomersByCountry(IEnumerable<CustomerCountry> customerCountries)
        {
            Console.Clear();
            var enumerable = customerCountries as CustomerCountry[] ?? customerCountries.ToArray();
            if (enumerable.Any())
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($" {"COUNTRY",-20}| {"COUNT",-10}|\n");
        
                foreach (CustomerCountry cc in enumerable)
                {
                    sb.Append($" {cc.Country,-20}| {cc.Count,-10}|\n");
                }
                
                TextColorManager.SetColor(ConsoleColor.DarkGreen);
                Console.WriteLine(sb.ToString());
                TextColorManager.ResetColor();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}