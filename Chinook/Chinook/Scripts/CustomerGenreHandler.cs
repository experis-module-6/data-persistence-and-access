﻿using System.Text;
using Chinook.Models;
using Chinook.Repositories;

namespace Chinook.Scripts
{
    public static class CustomerGenreHandler
    {
        private static readonly ICustomerGenreRepository Repository = new CustomerGenreRepository();

        /// <summary>
        /// Prints out a list of top genres for a specific customer in the console.
        /// </summary>
        /// <param name="id">CustomerId for the specific customer.</param>
        public static void ShowCustomerTopGenresById(int id)
        {
            try
            {
                List<CustomerGenre> customerGenres = Repository.GetCustomerGenreById(id.ToString());
                PrintGenres(customerGenres);
            }
            catch
            {
                TextColorManager.Warning("Did not find any genres for a customer with that ID.\n");
            }
        }

        private static void PrintGenres(IEnumerable<CustomerGenre> genres)
        {
            Console.Clear();
            var customerGenres = genres as CustomerGenre[] ?? genres.ToArray();
            if (customerGenres.Any())
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($"ID: {customerGenres[0].CustomerId}\n" +
                          $"Name: {customerGenres[0].FullName}\n" +
                          $"Top genre{(customerGenres.Length > 1 ? "s" : "")}: ");

                for (var i = 0; i < customerGenres.Length; ++i)
                {
                    sb.Append($"{(i > 0 ? ", " : "")}{customerGenres[i].Genre}");
                }

                sb.Append($"\n({customerGenres[0].GenreCount} results found)\n");

                TextColorManager.SetColor(ConsoleColor.DarkGreen);
                Console.WriteLine(sb.ToString());
                TextColorManager.ResetColor();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}