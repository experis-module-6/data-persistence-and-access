﻿namespace Chinook.Scripts
{
    public static class MenuManager
    {
        /// <summary>
        /// Initiates the menu logic.
        /// </summary>
        public static void StartProgram()
        {                
            TextColorManager.WriteInColor("Chinook Dashboard\n", ConsoleColor.Yellow);
            
            while (true)
            {
                Console.WriteLine("1. List all customers\n" +
                                  "2. Find a customer by ID\n" +
                                  "3. Find customers by name\n" +
                                  "4. Browse customer catalogue\n" +
                                  "5. Insert a new customer\n" +
                                  "6. Update an existing customer by ID\n" +
                                  "7. List countries based on customer count\n" +
                                  "8. Show top spenders\n" +
                                  "9. Show most popular genre(s) for a customer by ID\n\n" +
                                  "Esc. Quit");
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.D1:
                        Console.Clear();
                        CustomerDataHandler.SelectAll();
                        break;
                    case ConsoleKey.D2:
                        Console.Clear();
                        ShowSingleCustomer();
                        break;
                    case ConsoleKey.D3:
                        Console.Clear();
                        ShowNamePrompt();
                        break;
                    case ConsoleKey.D4:
                        Console.Clear();
                        ShowBrowseScreen();
                        break;
                    case ConsoleKey.D5:
                        Console.Clear();
                        ShowInsertPrompt();
                        break;
                    case ConsoleKey.D6:
                        Console.Clear();
                        ShowUpdatePrompt();
                        break;
                    case ConsoleKey.D7:
                        Console.Clear();
                        CustomerCountryDataHandler.ShowCustomersByCountryDescending();
                        break;
                    case ConsoleKey.D8:
                        Console.Clear();
                        CustomerSpendHandler.ShowBigSpendersDescending();
                        break;
                    case ConsoleKey.D9:
                        Console.Clear();
                        Console.WriteLine("Show customer top genre(s).");
                        CustomerGenreHandler.ShowCustomerTopGenresById(ShowIdPrompt());
                        break;
                    case ConsoleKey.Escape:
                        Console.Clear();
                        Console.WriteLine("Quitting...");
                        Environment.Exit(0);
                        break;
                    default:
                        Console.Clear();
                        break;
                }
            }
        }

        private static int ShowIdPrompt()
        {
            int selectedId;
            while (true)
            {
                Console.WriteLine("Insert customer id:");
                if (int.TryParse(Console.ReadLine(), out selectedId))
                {
                    break;
                }
                else
                {
                    Console.Clear();
                }
            }

            return selectedId;
        }

        static void ShowSingleCustomer()
        {
            Console.WriteLine("Show data for a single customer.");
            CustomerDataHandler.SelectSingle(ShowIdPrompt());
            Console.WriteLine();
        }

        static void ShowNamePrompt()
        {
            Console.WriteLine("Find customers by name (only partial match is necessary).");
            Console.WriteLine("Insert customer name:");
            string? selectedName = Console.ReadLine();
            if (selectedName is { Length: > 0 })
            {
                try
                {
                    CustomerDataHandler.SelectAllByName(selectedName);
                }
                catch
                {
                    Console.Clear();
                    TextColorManager.Warning($"Did not find any customers with name '{selectedName}'.\n");
                }
            }
            else
            {
                Console.Clear();
            }
        }

        static void ShowBrowseScreen()
        {
            CustomerDataHandler.OpenFirstPage();
            bool returnToMainMenu = false;
            while (!returnToMainMenu)
            {
                Console.WriteLine("Arrow keys: Change page\n\n" +
                                  "Esc. Return\n");
                switch (Console.ReadKey(true).Key)
                {
                    case ConsoleKey.UpArrow:
                    case ConsoleKey.RightArrow:
                        Console.Clear();
                        try
                        {
                            CustomerDataHandler.NextPage();
                            CustomerDataHandler.IsLastPage = false;
                        }
                        catch
                        {
                            CustomerDataHandler.IsLastPage = true;
                            Console.WriteLine($"Reached last page.\n");
                        }

                        break;
                    case ConsoleKey.DownArrow:
                    case ConsoleKey.LeftArrow:
                        Console.Clear();
                        try
                        {
                            CustomerDataHandler.PreviousPage();
                        }
                        catch
                        {
                            Console.WriteLine($"Reached first page.\n");
                        }

                        break;
                    case ConsoleKey.Escape:
                        Console.Clear();
                        returnToMainMenu = true;
                        break;
                }
            }
        }

        static void ShowInsertPrompt()
        {
            bool creatingCustomer = true;
            while (creatingCustomer)
            {
                string? firstName = "";
                while (string.IsNullOrEmpty(firstName) || firstName.Length > 40)
                {
                    Console.Clear();
                    Console.WriteLine("Insert first name:\n(Required, max length 40)");
                    firstName = Console.ReadLine();
                }

                string? lastName = "";
                while (string.IsNullOrEmpty(lastName) || lastName.Length > 20)
                {
                    Console.Clear();
                    Console.WriteLine("Insert last name:\n(Required, max length 20)");
                    lastName = Console.ReadLine();
                }

                Console.Clear();
                Console.WriteLine("Insert country:\n(Optional, max length 40)");
                var country = Console.ReadLine();
                while (!string.IsNullOrEmpty(country) && country.Length > 40)
                {
                    Console.Clear();
                    Console.WriteLine("Insert country:\n(Optional, max length 40)");
                    country = Console.ReadLine();
                }

                Console.Clear();
                Console.WriteLine("Insert postal code:\n(Optional, max length 10)");
                var postalCode = Console.ReadLine();
                while (!string.IsNullOrEmpty(postalCode) && postalCode.Length > 10)
                {
                    Console.Clear();
                    Console.WriteLine("Insert postal code:\n(Optional, max length 10)");
                    postalCode = Console.ReadLine();
                }

                Console.Clear();
                Console.WriteLine("Insert phone number:\n(Optional, max length 24)");
                var phone = Console.ReadLine();
                while (!string.IsNullOrEmpty(phone) && phone.Length > 24)
                {
                    Console.Clear();
                    Console.WriteLine("Insert phone number:\n(Optional, max length 24)");
                    phone = Console.ReadLine();
                }

                Console.Clear();
                string? email = "";
                while (string.IsNullOrEmpty(email) || email.Length > 60)
                {
                    Console.Clear();
                    Console.WriteLine("Insert email address:\n(Required, max length 60)");
                    email = Console.ReadLine();
                }

                country = string.IsNullOrEmpty(country) ? "" : country;
                postalCode = string.IsNullOrEmpty(postalCode) ? "" : postalCode;
                phone = string.IsNullOrEmpty(phone) ? "" : phone;

                Console.Clear();
                CustomerDataHandler.InsertNewCustomer(firstName, lastName, email, country, postalCode, phone);
                creatingCustomer = false;
            }
        }

        static void ShowUpdatePrompt()
        {
            bool updatingCustomer = true;

            while (updatingCustomer)
            {
                Console.WriteLine("Update customer data.");
                int customerId = ShowIdPrompt();

                Console.Clear();
                var selectedCustomer = CustomerDataHandler.GetSingleCustomer(customerId);
                bool selecting = true;

                if (selectedCustomer == null)
                {
                    Console.WriteLine();
                    break;
                }
                
                while (selecting && selectedCustomer != null)
                {
                    Console.WriteLine("\nWhich field do you want to update?\n\n" +
                                      "1. First Name\n" +
                                      "2. Last Name\n" +
                                      "3. Country\n" +
                                      "4. Postal Code\n" +
                                      "5. Phone number\n" +
                                      "6. Email address\n\n" +
                                      "Esc. Return\n");

                    switch (Console.ReadKey(true).Key)
                    {
                        case ConsoleKey.D1:
                            string? newFirstName = "";
                            while (string.IsNullOrEmpty(newFirstName) || newFirstName.Length > 40)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new first name:\n(Required, max length 40)");
                                newFirstName = Console.ReadLine();
                            }

                            selectedCustomer.FirstName = newFirstName;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.D2:
                            string? newLastName = "";
                            while (string.IsNullOrEmpty(newLastName) || newLastName.Length > 20)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new last name:\n(Required, max length 20)");
                                newLastName = Console.ReadLine();
                            }

                            selectedCustomer.LastName = newLastName;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.D3:
                            Console.Clear();
                            Console.WriteLine("Insert new country:\n(Optional, max length 40)");
                            var newCountry = Console.ReadLine();
                            while (newCountry?.Length > 40)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new country:\n(Optional, max length 40)");
                                newCountry = Console.ReadLine();
                            }

                            selectedCustomer.Country = string.IsNullOrEmpty(newCountry) ? "" : newCountry;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.D4:
                            Console.Clear();
                            Console.WriteLine("Insert new postal code:\n(Optional, max length 10)");
                            var newPostalCode = Console.ReadLine();
                            while (!string.IsNullOrEmpty(newPostalCode) && newPostalCode.Length > 10)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new postal code:\n(Optional, max length 10)");
                                newPostalCode = Console.ReadLine();
                            }

                            selectedCustomer.PostalCode = string.IsNullOrEmpty(newPostalCode) ? "" : newPostalCode;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.D5:
                            Console.Clear();
                            Console.WriteLine("Insert new phone number:\n(Optional, max length 24)");
                            var newPhone = Console.ReadLine();
                            while (!string.IsNullOrEmpty(newPhone) && newPhone.Length > 24)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new phone number:\n(Optional, max length 24)");
                                newPhone = Console.ReadLine();
                            }

                            selectedCustomer.Phone = string.IsNullOrEmpty(newPhone) ? "" : newPhone;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.D6:
                            Console.Clear();
                            Console.WriteLine("Insert new email address:\n(Required, max length 60)");
                            var newEmail = Console.ReadLine();
                            while (string.IsNullOrEmpty(newEmail) || newEmail.Length > 60)
                            {
                                Console.Clear();
                                Console.WriteLine("Insert new email address:\n(Required, max length 60)");
                                newEmail = Console.ReadLine();
                            }

                            selectedCustomer.Email = newEmail;
                            Console.Clear();
                            CustomerDataHandler.UpdateExistingCustomer(selectedCustomer);
                            break;
                        case ConsoleKey.Escape:
                            selecting = false;
                            break;
                    }

                    Console.Clear();
                    if (selecting)
                    {
                        selectedCustomer = CustomerDataHandler.GetSingleCustomer(customerId);
                    }
                }
                
                updatingCustomer = false;
            }
        }
    }
}