﻿using System.Text;
using Chinook.Models;
using Chinook.Repositories;

namespace Chinook.Scripts
{
    public static class CustomerDataHandler
    {
        #region BrowsePerPageVariables
        public static bool IsLastPage { get; set; }
        public const int CountPerPage = 10;
        private static int _currentPage;
        #endregion
        
        public static readonly ICustomerRepository Repository = new CustomerRepository();
        
        /// <summary>
        /// Prints out all existing customers in console.
        /// </summary>
        public static void SelectAll()
        {
            PrintCustomers(Repository.GetAllCustomers());
        }

        /// <summary>
        /// Opens the first page of customers in a paginated list and prints it out in console.
        /// </summary>
        public static void OpenFirstPage()
        {
            _currentPage = 0;
            PrintCustomers(Repository.GetAllCustomers(CountPerPage, _currentPage * 10));
        }

        /// <summary>
        /// Opens the next page of customers in a paginated list and prints it out in console.
        /// </summary>
        public static void NextPage()
        {
            if (!IsLastPage)
            {
                _currentPage++;
            }

            PrintCustomers(Repository.GetAllCustomers(CountPerPage, _currentPage * 10));
            Console.WriteLine($"Current page: {_currentPage + 1}");
        }

        /// <summary>
        /// Opens the previous page of customers in a paginated list and prints it out in console.
        /// </summary>
        public static void PreviousPage()
        {
            if (_currentPage > 0)
            {
                _currentPage--;
            }

            PrintCustomers(Repository.GetAllCustomers(CountPerPage, _currentPage * 10));
            Console.WriteLine($"Current page: {_currentPage + 1}");
        }

        /// <summary>
        /// Prints out the data for a single customer.
        /// </summary>
        /// <param name="id">customerId of the selected customer.</param>
        public static void SelectSingle(int id)
        {
            PrintCustomer(Repository.GetCustomerById(id.ToString()));
        }

        /// <summary>
        /// Prints out the data for a single customer.
        /// </summary>
        /// <param name="id">customerId of the selected customer.</param>
        /// <returns>A customer or null if none was found with the provided customerId.</returns>
        public static Customer? GetSingleCustomer(int id)
        {
            Customer? c = Repository.GetCustomerById(id.ToString());
            PrintCustomer(c);
            return c;
        }

        /// <summary>
        /// Prints out all the customers in console that meet the criteria.
        /// </summary>
        /// <param name="name">The name or part of a name that needs to appear in the customer name.</param>
        public static void SelectAllByName(string name)
        {
            List<Customer> customers = Repository.GetAllCustomersByName(name);
            if (customers.Any())
            {
                PrintCustomers(Repository.GetAllCustomersByName(name));
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Inserts a new customer to the database.
        /// </summary>
        /// <param name="firstName">First name.</param>
        /// <param name="lastName">Last name.</param>
        /// <param name="email">Email address.</param>
        /// <param name="country">Country.</param>
        /// <param name="postalCode">Postal code.</param>
        /// <param name="phoneNumber">Phone number.</param>
        public static void InsertNewCustomer(string firstName, string lastName, string email, string country = "",
            string postalCode = "", string phoneNumber = "")
        {
            Customer newCustomer = new Customer()
            {
                FirstName = firstName,
                LastName = lastName,
                Country = country,
                Email = email,
                Phone = phoneNumber,
                PostalCode = postalCode
            };

            if (Repository.AddNewCustomer(newCustomer))
            {
                // Console.WriteLine("Customer add succeeded!");
            }
            else
            {
                Console.WriteLine("Customer add failed!");
            }
        }

        /// <summary>
        /// Updates a customer in the database.
        /// </summary>
        /// <param name="customerWithNewValues">The updated parameters for the customer in a Customer type.</param>
        public static void UpdateExistingCustomer(Customer customerWithNewValues)
        {
            if (Repository.UpdateCustomer(customerWithNewValues))
            {
                Console.WriteLine("Customer update succeeded!");
            }
            else
            {
                Console.WriteLine("Customer add failed!");
            }
        }

        /// <summary>
        /// Prints out a list of customers in the console as a table with a header.
        /// </summary>
        /// <param name="customers">A list of customers.</param>
        /// <exception cref="Exception"></exception>
        private static void PrintCustomers(IEnumerable<Customer>? customers)
        {
            Console.Clear();
            if (customers != null)
            {
                StringBuilder sb = new StringBuilder();
                sb.Append(
                    $" {"ID",-5}| {"NAME",-31}| {"COUNTRY",-20}| {"POSTAL CODE",-15}| {"PHONE",-25}| {"EMAIL",-35}|\n");

                foreach (Customer customer in customers)
                {
                    sb.Append($" {customer.CustomerId,-5}| {customer.FirstName + " " + customer.LastName,-31}|" +
                              $" {customer.Country,-20}| {customer.PostalCode,-15}| {customer.Phone,-25}| {customer.Email,-35}|\n");
                }

                TextColorManager.SetColor(ConsoleColor.DarkGreen);
                Console.WriteLine(sb.ToString());
                TextColorManager.ResetColor();
            }
            else
            {
                throw new Exception();
            }
        }

        /// <summary>
        /// Prints out the data for a single customer in the console.
        /// </summary>
        /// <param name="customer">The customer.</param>
        private static void PrintCustomer(Customer? customer)
        {
            Console.Clear();
            if (customer == null)
            {
                TextColorManager.Warning("Did not find a customer with that ID.");
            }
            else
            {
                string data = string.Format($"ID: {(customer.CustomerId)}\n" +
                                  $"Name: {customer.FirstName} {customer.LastName}\n" +
                                  $"Country: {customer.Country}\n" +
                                  $"Postal Code: {customer.PostalCode}\n" +
                                  $"Phone: {customer.Phone}\n" +
                                  $"Email: {customer.Email}");
                TextColorManager.WriteInColor(data, ConsoleColor.DarkGreen);
            }
        }
    }
}