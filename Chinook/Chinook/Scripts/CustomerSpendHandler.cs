﻿using System.Text;
using Chinook.Models;
using Chinook.Repositories;

namespace Chinook.Scripts
{
    public static class CustomerSpendHandler
    {
        private static readonly ICustomerSpendRepository Repository = new CustomerSpendRepository();

        /// <summary>
        /// Prints out a list of all customers ordered by the total amount of money spent in a descending order.
        /// </summary>
        public static void ShowBigSpendersDescending()
        {
            List<CustomerSpender> spenders = Repository.GetTopSpenders();
            PrintBigSpenders(spenders);
        }

        private static void PrintBigSpenders(IEnumerable<CustomerSpender> spenders)
        {
            Console.Clear();
            var customerSpenders = spenders as CustomerSpender[] ?? spenders.ToArray();
            if (customerSpenders.Any())
            {
                StringBuilder sb = new StringBuilder();
                sb.Append($" {"ID",-5}| {"NAME",-31}| {"TOTAL SPENT", -12}|\n");
        
                foreach (CustomerSpender spender in customerSpenders)
                {
                    sb.Append($" {spender.CustomerId,-5}| " +
                              $"{spender.FirstName + " " + spender.LastName,-31}|" +
                              $" {spender.MoneySpent,-12}|\n");
                }
                
                TextColorManager.SetColor(ConsoleColor.DarkGreen);
                Console.WriteLine(sb.ToString());
                TextColorManager.ResetColor();
            }
            else
            {
                throw new Exception();
            }
        }
    }
}