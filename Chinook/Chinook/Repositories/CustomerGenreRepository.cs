﻿using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class CustomerGenreRepository : ICustomerGenreRepository
    {
        /// <summary>
        /// Generates a list of top genres for a specific customer based on their customerId.
        /// </summary>
        /// <returns>A list in CustomerGenre format.</returns>
        public List<CustomerGenre> GetCustomerGenreById(string customerId)
        {
            List<CustomerGenre> customerGenres = new List<CustomerGenre>();
            
                string sql =
                    $"WITH Data AS " +
                        $"(SELECT CustomerData.CustomerId as ID, CustomerData.FirstName as First, CustomerData.LastName as Second, CustomerData.Genre as Genre, CustomerData.GenreCount as Count " +
                    $"FROM " +
                    $"(SELECT Invoice.CustomerId as CustomerId, Customer.FirstName as FirstName, Customer.LastName as LastName, Genre.Name as Genre,COUNT(*) as GenreCount " +
                        $"FROM Invoice, InvoiceLine, Track, Genre, Customer WHERE Invoice.InvoiceId = InvoiceLine.InvoiceId AND InvoiceLine.TrackId = Track.TrackId AND Track.GenreId = Genre.GenreId And Invoice.CustomerId = Customer.CustomerId AND Customer.CustomerId = {customerId} " +
                        $"GROUP BY Invoice.CustomerId, Customer.FirstName, Customer.LastName, Genre.Name) AS CustomerData)" +
                    $"SELECT ID, First + ' ' + Second as Name, Genre, Count " +
                    $"FROM Data " +
                    $"WHERE Count = (SELECT MAX(Count) FROM Data)";
                
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerGenre temp = new CustomerGenre();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FullName = reader.GetString(1);
                                temp.Genre = reader.GetString(2);
                                temp.GenreCount = reader.GetInt32(3).ToString();
                                customerGenres.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customerGenres;
        }
    }
}