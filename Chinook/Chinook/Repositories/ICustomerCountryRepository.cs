﻿using Chinook.Models;

namespace Chinook.Repositories
{
    public interface ICustomerCountryRepository
    {
        public List<CustomerCountry> GetCustomersByCountry();
    }
}