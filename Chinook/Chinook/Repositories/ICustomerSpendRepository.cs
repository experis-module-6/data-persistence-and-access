﻿using Chinook.Models;

namespace Chinook.Repositories
{
    public interface ICustomerSpendRepository
    {
        public List<CustomerSpender> GetTopSpenders();
    }
}