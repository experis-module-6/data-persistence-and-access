﻿using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class CustomerCountryRepository : ICustomerCountryRepository
    {
        /// <summary>
        /// Generates a list of customer count per country in a descending order.
        /// </summary>
        /// <returns>A list in CustomerCountry format.</returns>
        public List<CustomerCountry> GetCustomersByCountry()
        {
            List<CustomerCountry> customerCountryList = new List<CustomerCountry>();

            string sql = $"SELECT Country, Count(*) as Count FROM Customer GROUP BY Country ORDER BY Count DESC";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                CustomerCountry temp = new CustomerCountry();
                                temp.Country = reader.GetString(0);
                                temp.Count = reader.GetInt32(1);
                                customerCountryList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            
            return customerCountryList;
        }
    }
}