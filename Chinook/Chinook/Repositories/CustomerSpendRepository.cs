﻿using System.Globalization;
using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class CustomerSpendRepository : ICustomerSpendRepository
    {
        /// <summary>
        /// Generates a list of customers ordered by total money spent in a descending order.
        /// </summary>
        /// <returns>A list of customers in CustomerSpender format.</returns>
        public List<CustomerSpender> GetTopSpenders()
        {
            List<CustomerSpender> spenders = new List<CustomerSpender>();

            string sql = 
                "WITH TotalSpending AS (select CustomerId, SUM(Total) AS TotalSpent FROM Invoice GROUP BY CustomerId) " +
                "SELECT Customer.CustomerId, FirstName, LastName, TotalSpent " +
                "FROM Customer, TotalSpending WHERE Customer.CustomerId = TotalSpending.CustomerId ORDER BY TotalSpent DESC";

            // Alternative way by using INNER JOIN
            // string sql = "SELECT Customer.CustomerId, Customer.FirstName, Customer.LastName, SUM(Invoice.Total) as TotalSpent " +
            //                       "FROM Invoice INNER JOIN Customer ON Invoice.CustomerId = Customer.CustomerId " +
            //                       "GROUP BY Customer.CustomerId, Customer.FirstName, Customer.LastName " +
            //                       "ORDER BY TotalSpent DESC";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            CultureInfo c = new CultureInfo("en-EN");

                            while (reader.Read())
                            {
                                CustomerSpender temp = new CustomerSpender();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                // temp.MoneySpent = reader.GetDecimal(1).ToString(c);

                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.MoneySpent = reader.GetDecimal(3).ToString(c);
                                spenders.Add(temp);
                            }
                        }
                    }
                }
            }
            catch (SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return spenders;
        }
    }
}