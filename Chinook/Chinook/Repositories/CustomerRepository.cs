﻿using System.Data;
using Chinook.Models;
using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Generates a list of all customers.
        /// </summary>
        /// <param name="maxCount">Amount of customers to fetch.</param>
        /// <param name="offset">Offset from the beginning of the Customer table.</param>
        /// <returns>A list of customers in Customer format.</returns>
        public List<Customer> GetAllCustomers(int maxCount = 1000, int offset = 0)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                         $"FROM Customer ORDER BY CustomerId OFFSET {offset} ROWS FETCH NEXT {maxCount} ROWS ONLY";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customerList;
        }
        
        /// <summary>
        /// Generates a list of customers whose full name match with the search string. Partial match is sufficient and the query is case insensitive.
        /// </summary>
        /// <param name="selectedName">Name or part of a name.</param>
        /// <returns>A list of customers in Customer format.</returns>
        public List<Customer> GetAllCustomersByName(string selectedName)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                         $"FROM Customer " +
                         $"WHERE LOWER(CONCAT(FirstName, ' ', LastName)) LIKE LOWER('%{selectedName}%')";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customerList.Add(temp);
                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }
            
            return customerList;
        }
        
        /// <summary>
        /// Finds a single customer based on their unique customerId.
        /// </summary>
        /// <param name="id">The customerId of the customer.</param>
        /// <returns>A customer or null if no customer was found with the customerId.</returns>
        public Customer? GetCustomerById(string id)
        {
            Customer? customer = null;
            string sql = $"SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email " +
                         $"FROM Customer " +
                         $"WHERE CustomerId = {id}";
            try
            {
                // Connect
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                Customer temp = new Customer();
                                temp.CustomerId = reader.GetInt32(0).ToString();
                                temp.FirstName = reader.GetString(1);
                                temp.LastName = reader.GetString(2);
                                temp.Country = reader.IsDBNull(3) ? "<missing>" : reader.GetString(3);
                                temp.PostalCode = reader.IsDBNull(4) ? "<missing>" : reader.GetString(4);
                                temp.Phone = reader.IsDBNull(5) ? "<missing>" : reader.GetString(5);
                                temp.Email = reader.GetString(6);
                                customer = temp;
                            }
                        }
                    }
                }
            }
            catch(SqlException ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            return customer;
        }
        
        /// <summary>
        /// Adds a new customer to the database.
        /// </summary>
        /// <param name="customer">The added Customer.</param>
        /// <returns>'true' if add was successful, 'false' if unsuccessful.</returns>
        public bool AddNewCustomer(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)" +
                         "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";
            
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0;

                    }
                }

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return success;
        }

        /// <summary>
        /// Updates an existing customer in the database.
        /// </summary>
        /// <param name="customer">The updated data for the customer.</param>
        /// <returns>'true' if update was successful, 'false' if unsuccessful.</returns>
        public bool UpdateCustomer(Customer customer)
        {
            bool success = false;

            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country," +
                         "PostalCode = @PostalCode, Phone = @Phone, Email = @Email" +
                         " WHERE CustomerId = @CustomerId";
            try
            {
                using (SqlConnection conn = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    conn.Open();
                    using (SqlCommand cmd = new SqlCommand(sql, conn))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0;
                    }
                }

            }
            catch(Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return success;
        }

        /// <summary>
        /// NOT IMPLEMENTED.
        /// Delete a customer from the database.
        /// </summary>
        /// <param name="customerId">The customerId of the customer.</param>
        /// <returns>'true' if delete was successful, 'false' if unsuccessful.</returns>
        /// <exception cref="NotImplementedException"></exception>
        public bool DeleteCustomer(string customerId)
        {
            throw new NotImplementedException();
        }
    }
}