﻿using Chinook.Models;

namespace Chinook.Repositories
{
    public interface ICustomerGenreRepository
    {
        public List<CustomerGenre> GetCustomerGenreById(string id);
    }
}