using Chinook.Models;

namespace Chinook.Repositories
{
    public interface ICustomerRepository
    {
        public Customer? GetCustomerById(string id);
        public List<Customer> GetAllCustomersByName(string name);
        public List<Customer> GetAllCustomers(int maxCount = 1000, int offset = 0);
        public bool AddNewCustomer(Customer customer);
        public bool UpdateCustomer(Customer customer);
        public bool DeleteCustomer(string customerId);
    }
}