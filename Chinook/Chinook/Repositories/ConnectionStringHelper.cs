﻿using Microsoft.Data.SqlClient;

namespace Chinook.Repositories
{
    public static class ConnectionStringHelper
    {
        /// <summary>
        /// Connection string for initializing the sql connection.
        /// </summary>
        /// <returns>The connection string.</returns>
        public static string GetConnectionString()
        {
            SqlConnectionStringBuilder connectionStringBuilder = new SqlConnectionStringBuilder();
            connectionStringBuilder.DataSource = @".\SQLEXPRESS";
            connectionStringBuilder.InitialCatalog = "Chinook";
            connectionStringBuilder.IntegratedSecurity = true;
            connectionStringBuilder.Encrypt = false;
            return connectionStringBuilder.ConnectionString;
        }
    }
}