﻿Customer data required:

1. CustomerId (int)
2. FirstName (nvarchar(40))
3. LastName (nvarchar(20))
4. Country (nvarchar(40), can be null)
5. PostalCode (nvarchar(10), can be null)
6. Phone (nvarchar(24), can be null)
7. Email (nvarchar(60))

Functionality required:
1. Read all Customer data ✔
2. Read specific Customer data ✔
3. Read specific Customer(s) data by name ✔
4. Return a page of customers (use 'limit' and 'offset' in query) ✔
5. Add new Customer to database ✔
6. Update an existing Customer ✔
7. Return amount of Customers in each country, ordered in descending order ✔
8. Return highest spending Customers (based on total in invoice table) in descending order ✔
9. Return most popular genre for a given customer (based on tracks in invoice table), show multiple in case of a tie ✔
Note: Only the columns in Customer data that are listed above (CustomerId, FirstName, LastName...) are required.
