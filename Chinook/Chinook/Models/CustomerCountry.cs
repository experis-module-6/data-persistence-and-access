﻿namespace Chinook.Models
{
    /// <summary>
    /// POCO for data fetched from the Customer table to generate country based queries.
    /// </summary>
    public class CustomerCountry
    {
        public string Country { get; set; }
        public int Count { get; set; }
    }
}