﻿namespace Chinook.Models
{ 
    /// <summary>
    /// POCO for data fetched from the Customer, Invoice, InvoiceLine, Track and Genre tables
    /// to generate queries based on most popular genres per customer.
    /// </summary>
    public class CustomerSpender
    {
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string MoneySpent { get; set; }
    }
}