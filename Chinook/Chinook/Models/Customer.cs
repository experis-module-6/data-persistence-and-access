﻿namespace Chinook.Models
{
    /// <summary>
    /// POCO for data fetched from the Customer table to generate customer-specific queries.
    /// </summary>
    public class Customer
    {
        public string CustomerId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
    }
}