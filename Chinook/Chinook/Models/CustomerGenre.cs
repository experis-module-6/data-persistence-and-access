﻿namespace Chinook.Models
{
    /// <summary>
    /// POCO for data fetched from the Customer and Invoice tables to generate queries based on money spent.
    /// </summary>
    public class CustomerGenre
    {
        public string CustomerId { get; set; }
        public string FullName { get; set; }
        public string Genre { get; set; }
        public string GenreCount { get; set; }
    }
}